<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);

use \GatewayWorker\Lib\Gateway;
use think\Controller;
use think\Db;

//引入数据库


/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{
    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     *
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {
        // 向当前client_id发送数据
        //sendToClient($client_id, "Hello $client_id\r\n");
        // 向所有人发送
        //Gateway::sendToAll("$client_id login\r\n");
    }
    
   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
    public static function onMessage($client_id, $message)
    {
        $data = json_decode($message, true);    //解析接收数据
        if (!isset($_SESSION['id'])) {    //未登录
            if ($data["event"]=="login") {  //设备请求登陆
                //查询数据库，比对uid和token
                $sql = "select * from device where id=".$data['id']." and token='".$data['token']."'";
                $db = new PDO("mysql:host=localhost;dbname=iot", 'test', 'test');
                $res = $db->exec('set names utf8');
                $res = $db->query($sql);
                if ($res->rowCount()>0) {  //登陆信息比对成功
                    //登陆成功日志记录
                    $sql = "insert into log(level,msg) values(0,'"."设备".$data['id']."登陆"."')";
                    $res = $db->query($sql);
                    $_SESSION['id']=$data['id'];
                    //查询设备的数据节点
                    // $sql = "select * from node where device_id='".$data['id']."'";
                    // $res = $db->query($sql);
                    // $list=array();
                    // while ($row = $res->fetch(PDO::FETCH_OBJ)) {
                    //     unset($row->dev_id);
                    //     $row->value=null;
                    //     $list[$row->id]=$row;
                    // }
                    // $_SESSION['node_list']= $list;//数据节点对象数组
                    Gateway::bindUid($client_id, $data['id']);//绑定设备ID
                    return Gateway::sendToCurrentClient(json_encode(array ('event'=>"login",'data'=>"success")));
                } else //登陆失败
                {
                    return Gateway::sendToCurrentClient(json_encode(array ('event'=>"login",'data'=>"failed")));
                }
            }
        } else //已登录
        {
            //具体业务逻辑，比如接受终端上报的数据
            switch ($data["event"]) {
                case "report":
                    $_SESSION['data']= $data['data'];//数据节点对象数组
                    // 这边是为了剔除上报的无用信息，现在准备放到用的时候再去处理
                    // foreach ($data['data'] as $item) {
                    //     if (array_key_exists(strval($item['id']), $_SESSION['node_list'])) {
                            
                    //         $_SESSION['node_list'][strval($item['id'])]->value=$item['value'];
                    //         //print_r($_SESSION['node_list'][strval($item['id'])]->value);
                    //     }
                    // }
                    /* TODO
                     * 将上报的数据插入到数据库
                     */
                    // $db = new PDO("mysql:host=localhost;dbname=iot", 'test', 'test');
                    // $sql="INSERT INTO user(username,password) values('".$data['uid']."','".strval($data['data'])."')";
                    // if($db->exec($sql))
                    //print_r($_SESSION['node_list']);
                    print("device ".$_SESSION['id']." report data:".json_encode($data['data'])."\n");
                    break;
            }
        }
            return;
    }

   
   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
    public static function onClose($client_id)
    {
        // 设备断开时日志记录
        $sql = "insert into log(level,message) values(0,'"."设备".$data['id']."退出登陆"."')";
        $db = new PDO("mysql:host=localhost;dbname=iot", 'test', 'test');
        $res = $db->exec('set names utf8');
        $res = $db->query($sql);
    }
}
