<?php

namespace app\api\controller;

use think\Request;
use think\Db;
use think\Controller;
use GatewayClient\Gateway;
use think\exception\HttpResponseException;

class Log extends Controller
{
    protected $beforeActionList = [
        'CheckLogin'
    ];

    protected function CheckLogin()
    {
        if (!session('user.id')) {
            throw new HttpResponseException(json(["code"=>2,"msg"=>"未登录","data"=>null]));
        }
    }
    /**
     * 显示资源列表
     * 设备列表
     * @return \think\Response
     */
    public function index()
    {
        $log = Db::table('log')->select();
        if ($log) {
            return json(["code"=>0,"msg"=>"获取日志成功","data"=>$log]);
        } else {
            return json(["code"=>1,"msg"=>"数据库操作异常","data"=>$null]);
        }
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
    }

    /**
     * 保存新建的资源
     * 创建新设备
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
       
    }


    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //判断设备是否属于自己
        $res = model('Log')->get(['id'=>$id,'user_id'=>session('user.id')]);
        if ($res) {
            model('Device')->destroy($id);
            return json(["code"=>0,"msg"=>"删除设备成功","data"=>null]);
        }
        return json(["code"=>1,"msg"=>"删除设备失败","data"=>null]);
    }
}
