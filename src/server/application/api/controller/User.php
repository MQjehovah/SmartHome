<?php

namespace app\api\controller;

use think\Db;
use think\Request;
use think\Controller;
use think\exception\HttpResponseException;

class User extends Controller
{
    protected $beforeActionList = [
        'CheckLogin' =>  ['only'=>'read,delete']
    ];

    protected function CheckLogin()
    {
        if (!session('user.id')) {
            throw new HttpResponseException(json(["code"=>2,"msg"=>"未登录","data"=>null]));
        }
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     * 注册新用户
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if (!empty($request->post('name')) && !empty($request->post('password'))) {
            if(Db::table('user')->insert([
                'name' => $request->post('name'),
                'password' => $request->post('password')
            ]))
            {
                return json(["code"=>0,"msg"=>"注册成功","data"=>null]);
            }
            else
            {
                return json(["code"=>1,"msg"=>"数据库操作异常","data"=>null]);
            }
            /**TODO
             * 注册成功与否还需判断，username字段不能重复
             */
        }
        return json(["code"=>4,"msg"=>"参数错误","data"=>null]);
    }

    /**
     * 显示指定的资源
     * 获取用户信息
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        if ($id == session('user.id')) {//只能获取本账户信息
            $data =Db::table('User')->where('id',$id)->find();//只能获取本ID
            if($data)
            {
                return json(['code'=>0,"msg"=>"获取用户信息成功","data"=>$data]);
            }
            else
            {
                return json(['code'=>1,"msg"=>"读取数据库失败","data"=>null]);
            }
           
        }
    }
        

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     * 删除用户
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if ($id == session('user.id')) {//只能删除本账户
            Db::table('User')->delete($id);
            session('user', null);
            return json(['code'=>0,"msg"=>"删除成功","data"=>null]);
        }
    }
}
