<?php

namespace app\api\controller;

use think\Db;
use think\Request;
use think\Controller;
use GatewayClient\Gateway;
use think\exception\HttpResponseException;

class Node extends Controller
{
    protected $beforeActionList = [
        'CheckLogin'
    ];


    protected function CheckLogin()
    {
        if (!session('user.id')) {
            throw new HttpResponseException(json(["code"=>2,"msg"=>"未登录","data"=>null]));
        }
    }

    /**
     * 显示资源列表
     * 或取数据节点列表
     * @return \think\Response
     */
    public function index()
    {
        if(input('?get.device_id'))
        {
            $nodes = Db::table('node')->where('device_id',input('get.device_id'))->select();
            $client_id = Gateway::getClientIdByUid(input('get.device_id'));
            $gateWaySession = Gateway::getSession($client_id[0]);
            foreach($nodes as &$item)
            {
                foreach($gateWaySession['data'] as $list)
                if($item['id']==$list['id'])
                {
                    $item['value']=$list['value'];
                }
            }
            $nodes[0]['type']="switch";
            $nodes[1]['type']="text";
            return json(["code"=>0,"msg"=>"","data"=>$nodes]);
        }
        return json(["code"=>1,"msg"=>"参数错误","data"=>$gateWaySession['data']]);
        // $client_id = Gateway::getClientIdByUid(session('user.id'));
        // $gateWaySession = Gateway::getSession($client_id[0]);
        // return json(["code"=>0,"msg"=>"","data"=>$gateWaySession['data']]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     * 节点控制指令
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
       //还不确定是用POST还是PUT，暂时先用POST方便调试（好像PUT好点，因为PUT带ID参数，可以指定设备）
       // dump(json_encode($request->post('data/a')));
        $data['event']="control";
        $data['data']=$request->post('data/a');
        // 不需要dev_id,要根据nodeid找到指定dev_id,-->用Put
        Gateway::sendToUid($request->post('dev_id'), json_encode($data));
        return json(["code"=>0,"msg"=>"","data"=>null]);
    }

    /**
     * 显示指定的资源
     * 获取指定ID的设备数据
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        $client_id = Gateway::getClientIdByUid(session('user.id'));
        $gateWaySession = Gateway::getSession($client_id[0]);
        foreach($gateWaySession['data'] as $item)
        {
            if($item['id']==$id)
            {
                return json(["code"=>0,"msg"=>"获取数据成功","data"=>$item['value']]);
            }
        }
        return json(["code"=>1,"msg"=>"获取数据失败","data"=>[strval($id)]]);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     * 修改id指定节点的数据（控制）
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        /*TODO
         * 待验证
         */
        $node = Db::table('node')->where('id', $id)->find();//找到所属接点
        $dev = Db::table('device')->where('id', $node['device_id'])->find();//找到所属设备
        if($dev['user_id']==session('user.id'))
        {
            $data['event']="control";
            $data['node_id']=intval($id);
            $data['value']=input('put.value/b');
            dump(json_encode($data));
            // $data['data'][0]=array('node_id'=>intval($id),'value'=>intval($request->post('value')));
            // // 不需要dev_id,要根据nodeid找到指定dev_id,-->用Put
            
            Gateway::sendToUid($dev['id'], json_encode($data));
            return json(["code"=>0,"msg"=>"","data"=>null]);
        }
        return json(["code"=>4,"msg"=>"没有操作权限","data"=>null]);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
