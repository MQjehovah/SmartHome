<?php

namespace app\api\controller;

use think\Db;
use think\Request;
use think\Controller;
use GatewayClient\Gateway;
use think\exception\HttpResponseException;

class Device extends Controller
{
    protected $beforeActionList = [
        'CheckLogin'
    ];

    protected function CheckLogin()
    {
        if (!session('user.id')) {
            throw new HttpResponseException(json(["code"=>2,"msg"=>"未登录","data"=>null]));
        }
    }
    /**
     * 显示资源列表
     * 设备列表
     * @return \think\Response
     */
    public function index()
    {
        $dev = Db::table('device')->where('user_id', session('user.id'))->select();
        if ($dev===null) {
            return json(["code"=>1,"msg"=>"未获取设备信息","data"=>null]);
        } else {
            foreach ($dev as &$item) {
                unset( $item['user_id']);
                //$item['status'] = "离线";
                $item['status']=Gateway::isUidOnline($item['id'])?"在线":"离线";
            }
            return json(["code"=>0,"msg"=>"获取设备列表成功","data"=>$dev]);
        }
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
    }

    /**
     * 保存新建的资源
     * 创建新设备
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if (!empty($request->post('name'))) {
            $devinfo = array('token'=>md5(date('Y-m-d', time()).session('user.id').$request->post('name')),//计算Token
            'name'=>$request->post('name'),
            'user_id'=>session('user.id'));
            if (Db::table('device')->insert($devinfo)) {
                return json(["code"=>0,"msg"=>"创建设备成功","data"=>null]);
            } else {
                return json(["code"=>1,"msg"=>"创建设备失败","data"=>null]);
            }
        }
        return json(["code"=>4,"msg"=>"参数错误","data"=>null]);
    }


    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //判断设备是否属于自己
        if (Db::table('device')->where('id', $id)->where('user_id', session('user.id'))->delete()) {
            return json(["code"=>0,"msg"=>"删除设备成功","data"=>null]);
        }
        return json(["code"=>1,"msg"=>"删除设备失败","data"=>null]);
    }
}
