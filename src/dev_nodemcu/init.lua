-- init.lua --

-- Start Info
print('===================')
print('\nMQ SmartHome\n')
print('===================')

-- Configure Wireless Internet
wifi.setmode(wifi.STATION)
print('set mode=STATION (mode='..wifi.getmode()..')\n')
print('MAC Address: ',wifi.sta.getmac())
print('Chip ID: ',node.chipid())
print('Heap Size: ',node.heap(),'\n')

-- Connected Callback
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, function(T)
    print("wifi connected")
    print("STA IP:"..T.IP)
    dofile("device.lua")
end)
require("config")

station_cfg={}
station_cfg.ssid=config.ssid
station_cfg.pwd=config.password
wifi.sta.config(station_cfg)

--------------------------
--按键扫描
--------------------------
--[[
    SC_BTN = 2
keyCnt = 0
gpio.mode(SC_BTN,gpio.INPUT)
function smartConf()
    wifi.startsmart(6, function(ssid, password)
        print(string.format("Success. SSID:%s ; PASSWORD:%s", ssid, password))
    end)
end

function keyScan() 
    local key = gpio.read(SC_BTN) 
    if (key == 0) then 
        keyCnt = keyCnt + 1 
    else if(keyCnt > 10) then 
        print("key down")
        smartConf() 
        keyCnt = 0 
        end 
    end 
end 
tmr.alarm(2, 10, 1, keyScan)

]]

print("Watting for connect...")
-- dofile("main.lua")