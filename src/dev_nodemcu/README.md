# ESP8266-nodeMCU项目

## nodeMCU简介

1. nodeMCU原理图
![nodeMCU原理图](../../doc/pic/机智云开发板原理图.png)

2. nodeMCU引脚映射图
![nodeMCU引脚映射图](../../doc/pic/nodemcu管脚映射图.png)

## 项目文件简介

1. init.lua是lua语言的初始运行脚本，有点像c语言的main函数，程序开始运行是，首先会调用init.lua执行，在本项目中，init.lua主要完成WIFI的连接为接下来的程序提供正确的网络环境

2. device.lua是设备的主要运行脚本，本程序中，主要作用是检测是否连接到服务器，如果未连接会一直尝试连接到服务器，一旦连接到服务器后，就会定时向服务器上报数据节点数据

3. config_sample.lua是设备的示例配置文件，用户需要拷贝一份并重命名为config.lua并修改其中的数据，然后烧写到设备中，主要提供设备的配置信息（包括WIFI账号密码，设备ID，TOKEN）以及服务器的地址等

## 【附录】

工具：

- [NodeMCU在线编译](https://www.nodemcu-build.com/)

第三方资料：

- [NodeMCU项目地址](https://github.com/nodemcu/nodemcu-firmware) 

参考资料：

- [NodeMCU官方手册](http://nodemcu.readthedocs.io/en/master/en/build/)