// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css' // 默认主题
import axios from 'axios'

Vue.config.productionTip = false;
Vue.prototype.$axios = axios;
Vue.use(ElementUI)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: {
    App
  },
  created() {
    this.$axios.get('/api/session').then((res) => {
      if (res.data.code == 0) { //远程登陆了
        localStorage.setItem('user', JSON.stringify(res.data.data));
      } else {
        //否则需要先登录
        this.$router.push('/login');
      }
    });

  },
  watch: {
    "$route": 'checkLogin'
  },
  methods: {
    checkLogin() {
      if (this.$route.path == "/register") return; //注册界面不需要判断登录
      if (!localStorage.getItem('user')) {
        this.$router.push('/login')
      }
    }
  }
})
