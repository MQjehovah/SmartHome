import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/Login',
      component: resolve => require(['../components/page/Login.vue'], resolve)
    },
    {
      path: '/register',
      component: resolve => require(['../components/page/register.vue'], resolve)
    },
    {
      path: '/',
      redirect: "/index"
    },
    {
      path: '/index',
      component: resolve => require(['../components/common/Home.vue'], resolve),
      children: [{
          path: '/',
          component: resolve => require(['../components/page/index.vue'], resolve)
        },
        {
          path: '/index',
          component: resolve => require(['../components/page/index.vue'], resolve)
        },
        {
          path: '/device_manager',
          component: resolve => require(['../components/page/device_manager.vue'], resolve)
        },
        {
          path: '/device_new',
          component: resolve => require(['../components/page/device_new.vue'], resolve)
        },
        {
          path: '/node_manager/:device_id',
          component: resolve => require(['../components/page/node_manager.vue'], resolve)
        },
        {
          path: '/log',
          component: resolve => require(['../components/page/log.vue'], resolve)
        },
        {
          path: '/basetable',
          component: resolve => require(['../components/page/basetable.vue'], resolve)
        }
      ]
    }
  ]
})
