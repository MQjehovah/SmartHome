# 智能家居项目

[设备端Nodemcu项目](./src/dev_nodemcu/README.md)

## 使用教程

### 设备端搭建

#### dev_nodemcu项目

- **编译**

>采用nodemcu提供的在线[编译工具](https://www.nodemcu-build.com/)

1. 填入邮箱
2. 选择master分支
3. 勾选ADC，Cron，DHT,file,GPIO,net,node,RTC time,SJSON,timer,UART,WIFI模块
4. 点击Start your build开始编译，几分钟之后，你将在填写的邮箱中收到编译好的文件

>doc文件夹中已经包含了下载好的固件[`nodemcu-master-11-modules-2017-10-05-18-12-33-float.bin`](./doc/nodemcu-master-11-modules-2017-10-05-18-12-33-float.bin)

- **烧写**

1. 使用esptool烧写

[esptool项目地址](https://github.com/espressif/esptool.git)

安装esptool.py：`pip install esptool`

烧写指令：`esptool.py --port <serial-port-of-ESP8266> write_flash -fm <mode> 0x00000 <nodemcu-firmware>.bin`
>mode is qio for 512 kByte modules and dio for >=4 MByte modules (qio might work as well, YMMV).

使用mq工具烧写：`mq run flash`

>mq工具的说明请参考[mq项目](https://gitee.com/MQjehovah/mq)

2. 使用PyFlasher烧写

[烧写工具](./tool/NodeMCU-PyFlasher-2.0.exe)

- **上传脚本**

1. 安装nodemcu-uploader
2. 上传指令：`nodemcu-uploader.py --port <serial-port-of-ESP8266> upload <uploader-file>`

>其他一些上传脚本的方法：
- [ESPlorer](https://esp8266.ru/esplorer/#download)
- [NodeMCU-Tool](https://github.com/andidittrich/NodeMCU-Tool)

### 服务端搭建

1. 克隆项目：`git clone https://gitee.com/MQjehovah/SmartHome.git`
2. 项目初始化：`composer install`

### 项目数据库初始化

使用mq工具，输入`mq run sql_setup`,按照提示输入数据库密码完成数据库初始化

## 【附录】

### 参考文档

- [ThinkPHP5官方手册](https://www.kancloud.cn/manual/thinkphp5/118003)
- [GateWayWorker官方手册](http://www.workerman.net/gatewaydoc/)
- [Element-ui官方手册](http://element.eleme.io/#/zh-CN/component/installation)
- [vue官方手册](https://cn.vuejs.org/v2/guide/)