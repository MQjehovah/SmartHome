grant all privileges on iot.* to 'test'@'%' identified by 'test';
grant all privileges on iot.* to 'test'@'localhost' identified by 'test';
flush privileges;
SET NAMES utf8;

source output.sql
source testdata.sql